# Dash1.Plugins.Db.Mnesia

# Usage

```elixir
first_run = Application.get_env(:dash1_plugins_db_mnesia, :creation_state, nil)
if first_run == :first_run do
  # Stuffing database with initial values
  IO.puts("firstrun")
else # :already_exist
  # Maintenance or whatever
  IO.puts("run_when_already_exist")
end
```

## Installation

```elixir
def deps do
  [
    {:dash1_plugins_db_mnesia, git: "git@gitlab.com:z5ottu/elixir-dash1-plugins-db-mnesia.git" , tag: "0.2.4"},
  ]
end
```
