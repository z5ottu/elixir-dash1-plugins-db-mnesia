defmodule Dash1.Plugins.Db.Mnesia do
    require Record

    @vsn "0.2.3"
    @mnesia_wait_for_tables_timeout 3_000

    alias :mnesia, as: Mnesia
    Record.defrecord :data, id: nil, type: nil, map: nil
    @type data :: record(:data, id: Integer.t, type: String.t, map: Map.t)

    # Kompatiblitias miatt.
    def init(_) do
        :ok
    end

    # def init() do
    #     init(nil)
    # end

    @doc """
    ## examples:

    - init(first_run: fn->IO.puts("firstrun") end)
    - init(run_when_already_exist: fn->IO.puts("run_when_already_exist") end)
    """
    def init() do
        IO.puts "Dash1.Plugins.Db.Mnesia init"
        x = :mnesia.system_info(:use_dir)
        if !x do
            IO.puts "Dash1.Plugins.Db.Mnesia init: SCHEMA: CREATE"
            Mnesia.stop()
            Mnesia.create_schema([node()])
            Mnesia.start()
        else
            IO.puts "Dash1.Plugins.Db.Mnesia init: SCHEMA: EXISTS"
        end

        {x,y} =
            case Mnesia.create_table(Seq, [disc_copies: [node()]]) do
                {:atomic, :ok} ->
                    IO.puts("Seq Table Created")
                    {:ok, :ok}
                {:aborted, {:already_exists, a}} ->
                    IO.puts("Seq Table #{inspect(a)} already exist")
                    {:ok, :ok}
                {:aborted, a} ->
                    IO.puts("Seq Table #{inspect(a)} ERROR!")
                    {:error, a}
                # xx ->
                #     IO.puts("Seq Table, unknown return #{inspect xx}")
                #     {:error, nil}
            end

        {x,y} =
            if x == :ok do
                case Mnesia.create_table(Data, [disc_copies: [node()], attributes: [:id, :type, :map]]) do
                    {:atomic, :ok} ->
                        IO.puts("Data Table Created")
                        Application.put_env(:dash1_plugins_db_mnesia, :creation_state, :first_run)
                        {:ok, :ok}
                    {:aborted, {:already_exists, a}} ->
                        IO.puts("Data Table #{inspect(a)} already exist")
                        Application.put_env(:dash1_plugins_db_mnesia, :creation_state, :already_exist)
                        {:ok, :ok}
                    {:aborted, a} ->
                        IO.puts("Data Table #{inspect(a)} ERROR!")
                        {:error, a}
                    # xx ->
                    #     IO.puts("Data Table, unknown return #{inspect xx}")
                    #     {:error, nil}
                end
            else
                {x,y}
            end # if x == :ok

        # Befejezes
        if x == :ok, do: :mnesia.wait_for_tables([Seq,Data], @mnesia_wait_for_tables_timeout)

        IO.puts "Dash1.Plugins.Db.Mnesia init finished"
        {x,y}

    end # def init

    @doc """
    returns a new seq number (system wide uniq)
    """
    def getNewId() do
        :mnesia.dirty_update_counter(Seq,:client,1)
    end

    #   @doc """
    #   write default
    #   """
    # def writeDefaults(func) do
    # 	try do
    #           if is_function func do
    #               func.()
    #           end
    # 	rescue
    # 	    _ -> nil
    # 	end
    # end

    @doc """
    select data from mnesia by type
    """
    def selectDataViaType(type) do
        case Mnesia.transaction(fn ->Mnesia.match_object({Data, :_, type, :_}) end) do
            {:atomic,a} -> a
            _ -> []
        end
    end

    @doc """
    getData by amnesia id
    """
    def getData(id) do
        case Mnesia.transaction(fn ->Mnesia.match_object({Data, id, :_, :_}) end) do
            {:atomic,a} ->
                if Enum.count(a) == 1 do
                    Enum.at(a,0)
                else
                    nil
                end
            _ -> nil
        end
    end

    @doc """
    getDataAttributes by amnesia id
    """
    def getDataAttributes(id) do
        getAttributesFromData(getData(id))
    end

    def getDatalistMapByType(type) do
        case selectDataViaType(type) do
            nil -> %{"datalist" => []}
            aa ->
                case type do
                    "page" ->
                        out = %{
                            "datalist" => Enum.map(aa,
                                fn(s) -> %{
                                             "id" => data(s,:id),
                                             "type" => data(s,:type),
                                             "name" => Map.get(data(s,:map),"name"),
                                             "url" => Map.get(data(s,:map),"url")
                                         } end)
                        }
                        count = if out["datalist"] |> is_list, do: out["datalist"] |> Enum.count
                        Map.put(out, "totalpages",count)
                    _ -> %{}
                end
        end
    end

    @doc """
    delDataViaId(id)
    del data by amnesia id
    """
    def delDataViaId(id) do
        f = fn()->Mnesia.delete({Data, id}) end
        Mnesia.activity(:sync_dirty,f)
    end

    @doc """
    getAttributesFromData(d)
    get attributes from data
    """
    def getAttributesFromData(d) do
        d = if is_list(d) do Enum.at(d,0) else d end
        data(d,:map)
    end

    @doc """
    getAttributesFromData(d,attribute)
    get attributes from data
    """
    def getAttributesFromData(d,attribute) do
        d = if is_list(d) do Enum.at(d,0) else d end
        case data(d,:map) do
            bb -> Map.get(bb,attribute)
        end
    end

    @doc """
    selectDataViaType(type,map)
    select data by type and map
    """
    def selectDataViaType(type,map) do
        case Mnesia.transaction(fn ->Mnesia.match_object({Data, :_, type, map}) end) do
            {:atomic,a} -> a
            _ -> []
        end
    end

    @doc """
    updateCreatePage(name,map)
    update or create page (default type is dynamic)
    """
    def updateCreatePage(name,map) do
        updateCreatePage(name,map,nil)
    end

    def updateCreatePage(name,map,type) do
        aa = Dash1.Plugins.Db.Mnesia.selectDataViaType("page",%{"name"=>name})
        case Enum.count(aa) do
            1 ->
                newmap = case type do
                    nil -> %{"name"=>name}
                    _newtype -> %{"name"=>name,"type" => type}
                end
                if tuple_size(Enum.at(aa,0)) > 1,
                   do:
                       Dash1.Plugins.Db.Mnesia.writeDataAttributes(elem(Enum.at(aa,0),1),Map.merge(newmap ,map))
            0 ->
                newmap = case type do
                    nil -> %{"name"=>name,"type" => "dynamic"}  # (default type is dynamic)
                    _newtype -> %{"name"=>name,"type" => type}
                end
                pagesId = Dash1.Plugins.Db.Mnesia.addPageContentEmpty
                Dash1.Plugins.Db.Mnesia.writeDataAttributes(pagesId,Map.merge(newmap,map))
            _ -> nil
        end
        uiModule = Application.get_env(:elixirdash1, :uiModule)
        uiModule.refreshPages()
    end

    @doc """
    setDataMapByName(name,map)
    update or create data by name (merge)
    """
    def setDataMapByName(name,map) do
        aa = Dash1.Plugins.Db.Mnesia.selectDataViaType("data",%{"name"=>name})
        case Enum.count(aa) do
            1 -> if tuple_size(Enum.at(aa,0)) > 1,
                    do:
                        Dash1.Plugins.Db.Mnesia.writeDataAttributes(elem(Enum.at(aa,0),1),Map.merge(%{"name"=>name},map))
            0 ->
                id = Dash1.Plugins.Db.Mnesia.addDataContentEmpty
                Dash1.Plugins.Db.Mnesia.writeDataAttributes(id,Map.merge(%{"name"=>name},map))
            _ -> nil
        end
    end

    @doc """
    delDataByName(name)
    delete data by name
    """
    def delDataByName(name) do
        aa = getDataByName(name)
        case Enum.count(aa) do
            1 -> if tuple_size(Enum.at(aa,0)) > 1,
                    do: delDataViaId(elem(Enum.at(aa,0),1))
            _ -> nil
        end
    end

    @doc """
    getDataByName(name)
    get mnesia record by name
    """
    def getDataByName(name) do
        Dash1.Plugins.Db.Mnesia.selectDataViaType("data",%{"name"=>name})
    end

    @doc """
    getDataMapByName(name)
    get datamap by name
    """
    def getDataMapByName(name) do
        case Dash1.Plugins.Db.Mnesia.selectDataViaType("data",%{"name"=>name}) do
            nil -> nil
            [] -> nil
            a -> Dash1.Plugins.Db.Mnesia.getAttributesFromData(a)
        end
    end
    @doc """
    getDataMapByName(name,attribute)
    get attribute data by name and attributename
    """
    def getDataMapByName(name,attribute) do
        case Dash1.Plugins.Db.Mnesia.selectDataViaType("data",%{"name"=>name}) do
            nil -> nil
            [] -> nil
            a -> Dash1.Plugins.Db.Mnesia.getAttributesFromData(a,attribute)
        end
    end

    @doc """
    writeDataAttributes(id,map)
    write/update datamap to id
    """
    def writeDataAttributes(id,map) do
        case getData(id) do
            nil -> false
            tData ->
                tmpMap = Map.merge(data(tData, :map),map)
                case Mnesia.transaction(fn -> Mnesia.write(data(tData, map: tmpMap)) end) do
                    {:atomic , _} -> true
                    _ -> false
                end
        end
    end

    @doc """
    addDataMap(map,type)
    """
    def addDataMap(map,type) do
        id = Dash1.Plugins.Db.Mnesia.getNewId()
        Dash1.Plugins.Db.Mnesia.addDataMap(map,id,type)
    end

    def addDataMap(map,id,type) do
        case Mnesia.transaction(fn -> Mnesia.write({Data, id, type, map}) end) do
            {:atomic, _} -> id
            {:aborted,_} -> false
            # _ -> false
        end
    end

    @doc """
    addDataContentEmpty
    returns a new id
    """
    def addDataContentEmpty do
        id = Dash1.Plugins.Db.Mnesia.getNewId()
        dataMapEmpty = %{"name" => "tmp_#{id}"}
        case Mnesia.transaction(fn -> Mnesia.write({Data, id, "data", dataMapEmpty}) end) do
            {:atomic, _} -> id
            {:aborted,_} -> false
            # _ -> false
        end
    end

    @doc """
    addPageContentEmpty
    returns a new id
    """
    def addPageContentEmpty do
        id = Dash1.Plugins.Db.Mnesia.getNewId()
        pageMapEmpty = %{"name" => "tmp_#{id}", "type"=>"static", "render" => ""}
        case Mnesia.transaction(fn -> Mnesia.write({Data, id, "page", pageMapEmpty}) end) do
            {:atomic, _} -> id
            {:aborted,_} -> false
            # _ -> false
        end
    end

    @doc """
    addWidgetEmpty
    returns a new id
    """
    def addWidgetEmpty do
        id = Dash1.Plugins.Db.Mnesia.getNewId()
        widgetMapEmpty = %{
            "name" => "widget_#{id}",
            "type"=>"static",
            "render" => """
                <div class="animated flipInY {{default_widget_width}}">
                <div class="tile-stats">
                <div class="icon"><i class="fa {{default_widget_icon}}"></i>
                </div>
                <div class="count">{{default_widget_count}}</div>
                <h3>{{default_widget_title}}</h3>
                <p>{{default_widget_desc}}</p>
                </div>
                </div>
            """
        }

        # igy lehetne tarolni a page-nel a widget-eket. kell az adatforrasok osszegyujtesehez is
        # %{"page" => [%{"type" => "row", "children" => %{"type" => "widget", "id"=> 6}}]}

        case Mnesia.transaction(fn -> Mnesia.write({Data, id, "widget", widgetMapEmpty}) end) do
            {:atomic, _} -> id
            {:aborted,_} -> false
            # _ -> false
        end
    end
end
