

defmodule Dash1.Plugins.Db.Mnesia.Application do
  use Application

  def start(_type, _args) do
    {x,y} = Dash1.Plugins.Db.Mnesia.init()
    if x == :ok do
      Dash1.Plugins.Db.Mnesia.Supervisor.start_link
    else
      {x,y}
    end
  end # def start

end # defmodule
