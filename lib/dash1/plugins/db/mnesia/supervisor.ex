alias Dash1.Plugins.Db.Mnesia

defmodule Mnesia.Supervisor do
	use Supervisor
	  @moduledoc """
	  Dummy supervisor, sajnos ha az `Application.start`-ot hasznaljuk, kell egy.
	  Ez nem csinal semmit.
	  """

	def start_link do
		Supervisor.start_link(__MODULE__, :ok)
	end # def start_link

	def init(:ok) do
		children = []
		supervise(children, strategy: :one_for_one)
	end # def init

end # defmodule
