defmodule Dash1.Plugins.Db.Mnesia.Mixfile do
  use Mix.Project

  @vsn "0.2.4"

  def project do
    [
      app: :dash1_plugins_db_mnesia,
      version: @vsn,
      elixir: "~> 1.4",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      deps: deps(),
    ]
  end

  def application do
    [
      applications: [
        :mnesia,
      ],
      extra_applications: [],
      mod: { Dash1.Plugins.Db.Mnesia.Application, [] },
    ]
  end

  defp deps do
    [

    ]
  end

end
